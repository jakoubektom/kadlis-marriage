# Init project
## Project download
* `git clone https://gitlab.com/jakoubektom/kadlis-marriage.git` - this will download project to folder `./kadlis-marriage`
## Project run
Run following commands in command line in project root
* `npm install` - install thd party libraries
* `npm start` - compile code and run server, any further change will be autoreloaded
* edit `./src/assets/data.js` and save - Kadlis life journey configuration
## Useful git commands
* `git add *` - add our work in progress files as staged to commit
* `git commit -m "Commit message"` - commit changes (obv)
* `git push` - push changes to remote server
* `git fetch` - fetch remote changes to local PC, but not to current workspace
* `git pull` - fetch and merge remote changes to local workspace
* `git rebase origin/master` - automagically somehow merge local changes with fetched changes in origin/master