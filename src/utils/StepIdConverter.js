export class StepIdConverter {
	static encode(id) {
		return window.btoa(id);
	}

	static decode(id) {
		try {
			return window.atob(id);
		} catch (e) {
			console.error(`Unable to decode string: `, id, e)
			return null;
		}
	}
}