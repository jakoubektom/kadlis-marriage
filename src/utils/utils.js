export const requiredDefined = (stepId, value, fieldLabel) => {
   if (value === undefined) {
      throw Error(`Step ${stepId} has not defined field ${fieldLabel}`);
   }
   return value;
};
