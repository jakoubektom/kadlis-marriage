import React from "react";
import { Spinner } from "reactstrap";
import styled from "styled-components";
import { bool } from "prop-types";

const Background = styled.div`
   position: fixed;
   width: 100%;
   top: 0;
   right: 0;
   bottom: 0;
   left: 0;
   background-color: rgba(255, 255, 255, 0.8);
   z-index: 999;
`;

const StyledSpinner = styled(Spinner)`
   position: fixed;
   top: 50%;
   right: 50%;
   z-index: 1000;
`;

export const PageSpinner = ({ display }) =>
   display ? (
      <div>
         <Background />
         <StyledSpinner animation="border" role="status" />
      </div>
   ) : null;

PageSpinner.propTypes = {
   display: bool.isRequired,
};
