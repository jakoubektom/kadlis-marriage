import React, { useCallback } from "react";
import styled from "styled-components";
import ReactFlow, { addEdge, ConnectionLineType, useEdgesState, useNodesState } from "react-flow-renderer";
import dagre from "dagre";
import { requiredDefined } from "../utils/utils";
import { story } from "../assets/story";
import { Button } from "reactstrap";
import { Color } from "../utils/Constants";
import { useStepFlowNavigate } from "../pages/flowPages/useStepFlowNavigate";

const position = { x: 0, y: 0 };
const edgeType = "smoothstep";

const Container = styled.div`
   flex-grow: 1;
   display: flex;

   .react-flow__node {
      background-color: inherit;
   }

   .react-flow__edge-textbg {
      fill: ${Color.BACKGROUND_DEFAULT};
   }

   .react-flow__handle {
      border-color: ${Color.BACKGROUND_DEFAULT};
   }

   .layoutflow {
      flex-grow: 1;
      position: relative;
      height: 100%;
   }

   .layoutflow .controls {
      position: absolute;
      right: 10px;
      top: 10px;
      z-index: 10;
      font-size: 12px;
   }

   .layoutflow .controls button:first-child {
      margin-right: 10px;
   }
`;

const getNodes = () => {
   return story.map((step) => {
      const id = requiredDefined("NaN", step.id, "step.id");
      const meta = requiredDefined(id, step.meta, "step.meta");
      if (!meta.label && !step.header) {
         throw Error(`In step ${id} meta.label or header must be defined`);
      }
      return {
         id: step.id,
         type: meta.type,
         data: {
            label: meta.label || step.header,
         },
         position,
      };
   });
};

String.prototype.hashCode = function() {
   var hash = 0, i, chr;
   if (this.length === 0) return hash;
   for (i = 0; i < this.length; i++) {
      chr   = this.charCodeAt(i);
      hash  = ((hash << 5) - hash) + chr;
      hash |= 0; // Convert to 32bit integer
   }
   return hash;
};

const getEdges = () => {
   return story.flatMap((step) => {
      const id = requiredDefined("NaN", step.id, "step.id");
      const navigation = requiredDefined(id, step.navigation, "step.navigation");

      return navigation.map((nav) => {
         requiredDefined(id, nav.goTo, "nav.goTo");

         return {
            id: `${id}-${nav.goTo}-${nav.label && nav.label.hashCode()}`,
            source: id,
            target: nav.goTo,
            type: edgeType,
            animated: true,
            label: nav.label,
         };
      });
   });
};

const initialEdges = getEdges();
const initialNodes = getNodes();

const dagreGraph = new dagre.graphlib.Graph();
dagreGraph.setDefaultEdgeLabel(() => ({}));

const nodeWidth = 172;
const nodeHeight = 36;

const getLayoutedElements = (nodes, edges, direction = "TB") => {
   const isHorizontal = direction === "LR";
   dagreGraph.setGraph({ rankdir: direction });

   nodes.forEach((node) => {
      dagreGraph.setNode(node.id, { width: nodeWidth, height: nodeHeight });
   });

   edges.forEach((edge) => {
      dagreGraph.setEdge(edge.source, edge.target);
   });

   dagre.layout(dagreGraph);

   nodes.forEach((node) => {
      const nodeWithPosition = dagreGraph.node(node.id);
      node.targetPosition = isHorizontal ? "left" : "top";
      node.sourcePosition = isHorizontal ? "right" : "bottom";

      // We are shifting the dagre node position (anchor=center center) to the top left
      // so it matches the React Flow node anchor point (top left).
      node.position = {
         x: nodeWithPosition.x - nodeWidth / 2,
         y: nodeWithPosition.y - nodeHeight / 2,
      };

      return node;
   });

   return { nodes, edges };
};

const { nodes: layoutedNodes, edges: layoutedEdges } = getLayoutedElements(initialNodes, initialEdges);

export const FlowChart = () => {
   const [nodes, setNodes, onNodesChange] = useNodesState(layoutedNodes);
   const [edges, setEdges, onEdgesChange] = useEdgesState(layoutedEdges);
   const goToFlowStep = useStepFlowNavigate();

   const onConnect = useCallback(
      (params) => setEdges((eds) => addEdge({ ...params, type: ConnectionLineType.SmoothStep, animated: true }, eds)),
      []
   );
   const onLayout = useCallback(
      (direction) => {
         const { nodes: layoutedNodes, edges: layoutedEdges } = getLayoutedElements(nodes, edges, direction);

         setNodes([...layoutedNodes]);
         setEdges([...layoutedEdges]);
      },
      [nodes, edges]
   );

   return (
      <Container>
         <div className="layoutflow">
            <ReactFlow
               nodes={nodes}
               edges={edges}
               onNodesChange={onNodesChange}
               onEdgesChange={onEdgesChange}
               onConnect={onConnect}
               connectionLineType={ConnectionLineType.SmoothStep}
               fitView
               onNodeDoubleClick={(e, node) => {
                  console.log(`Double clicked node, e: `, e, " node: ", node);
                  goToFlowStep(node.id);
               }}
            />
            <div className="controls">
               <Button onClick={() => onLayout("TB")} outline>
                  vertical layout
               </Button>
               <Button onClick={() => onLayout("LR")} outline>
                  horizontal layout
               </Button>
            </div>
         </div>
      </Container>
   );
};
