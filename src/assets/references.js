export const ImageId = {
   FRIENDS: "friends",
   GRANDPARENTS: "grandparents",
   COUNTRYSIDE: "countryside",
   TV: "tv",
   AMBULANCE: "ambulance",
};

export const ComponentId = {
   GENERIC: "generic", // default
   LOADING_BAR: "loading-bar",
   CHAPTER_END: "chapter-end",
};
