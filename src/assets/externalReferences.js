import { FstChapterId } from "./chapters/01-afterWeddingChapter";
import { BeforeStoryChapterId } from "./chapters/00-beforeStory";
import { ChildBirthChapterId } from "./chapters/02-childBirth";
import { RetirementChapterId } from "./chapters/04-retirement";

export const PART_0_START = BeforeStoryChapterId.STEP_INIT;
export const PART_1_START = FstChapterId.STEP_INIT;
export const PART_2_START = ChildBirthChapterId.STEP__POROD;
export const PART_4_START = RetirementChapterId.STEP__INIT;
