import { afterWeddingChapter } from "./chapters/01-afterWeddingChapter";
import { childBirthChapter } from "./chapters/02-childBirth";
import { childrenEducationChapter } from "./chapters/03-childrenEducation";
import { retirementChapter } from "./chapters/04-retirement";
import { beforeStoryChapter } from "./chapters/00-beforeStory";

export const story = [
   ...beforeStoryChapter,
   ...afterWeddingChapter,
   ...childBirthChapter,
   ...childrenEducationChapter,
   ...retirementChapter,
];
