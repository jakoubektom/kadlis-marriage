import { ComponentId, ImageId } from "../references";
import { Gifs } from "../pics";
import { createContinueNav, createNav } from "../chapterUtils";

export const FstChapterId = {
   STEP_INIT: "001",
   STEP_INIT_DOWNLOAD: "002",
   STEP__VOLBA_CESTY: "003",
   STEP__ROSSOVA_CESTA: "004",
   STEP__ROSSOVA_CESTA_ZACHRANA: "005",
   STEP__CHOSEN_GANDALF: "006",
   STEP__PRVNI_RANO: "007",
   STEP__PRVNI_RANO__AJA_REAKCE: "008",
   STEP__PRVNI_RANO__TV: "009",
   STEP__PRVNI_RANO__TV_DECIDE: "010",
   STEP__END: "099",
};

export const afterWeddingChapter = [
   {
      id: FstChapterId.STEP_INIT,
      header: "Cesta tam a už ne zpátky",
      content: "Vítejte na začátku své cesty, jen jeden dva kliky a můžete vykročit",
      navigation: [createNav("Download", FstChapterId.STEP_INIT_DOWNLOAD)],
      meta: {
         type: "input",
      },
   },
   {
      id: FstChapterId.STEP_INIT_DOWNLOAD,
      componentId: ComponentId.LOADING_BAR,
      header: "Downloading virus Marriage",
      navigation: [createContinueNav(FstChapterId.STEP__VOLBA_CESTY)],
      meta: {},
   },
   {
      id: FstChapterId.STEP__VOLBA_CESTY,
      header: "Volba cesty",
      content:
         "<div class='text-center'>" +
         "<p>Všichni na začátku máme možnost volby a nyní jste to vy, kdo stojí na začátku a volí!</p>" +
         "<p>Tak jaký život si zvolíte?</p>" +
         "</div>",
      navigation: [
         createNav("Hobití pohoda", FstChapterId.STEP__CHOSEN_GANDALF),
         createNav("Rossova cesta", FstChapterId.STEP__ROSSOVA_CESTA),
      ],
      meta: {},
   },
   {
      id: FstChapterId.STEP__ROSSOVA_CESTA,
      header: "Rossova cesta",
      content: `<div class='text-center'>
         <p>Ale no tak, to mi chcete říct, že si přejete pro Ondru pár rozvodů a z Áji udělat lesbičku?</p>
         </div>
`,
      navigation: [createContinueNav(FstChapterId.STEP__ROSSOVA_CESTA_ZACHRANA)],
      meta: {},
   },
   {
      id: FstChapterId.STEP__ROSSOVA_CESTA_ZACHRANA,
      content: "Naštěstí máte přátele, co vás kdyžtak podrží, takže alou na hobití cestu!",
      imageId: ImageId.FRIENDS,
      navigation: [createNav("Hobití cesta", FstChapterId.STEP__CHOSEN_GANDALF)],
      meta: { label: "ROSSOVA_CESTA_ZACHRANA" },
   },
   {
      id: FstChapterId.STEP__CHOSEN_GANDALF,
      header: "Dobré Jitro",
      content: `${Gifs.GANDALF}
        <div class="text-center">               
           <p>Dobrá volba</p>
        </div>
`,
      navigation: [createContinueNav(FstChapterId.STEP__PRVNI_RANO)],
      meta: {},
   },
   {
      id: FstChapterId.STEP__PRVNI_RANO,
      header: "První ráno",
      content: `
        <div class="text-center">               
           <p>Vaše první jitro po té velké změně.</p>
           <p>Významný den si žádá významné činy!</p>
           <p>Ondro, co uděláš?</p>
        </div>
`,
      navigation: [
         createNav("Svatba nesvatba, stále gentleman, udělám snídani", FstChapterId.STEP__PRVNI_RANO__AJA_REAKCE),
         createNav("Donesu projektovou dokumentaci našeho domečku", FstChapterId.STEP__PRVNI_RANO__AJA_REAKCE),
         createNav("Spím dál", FstChapterId.STEP__PRVNI_RANO__AJA_REAKCE),
      ],
      meta: {},
   },
   {
      id: FstChapterId.STEP__PRVNI_RANO__AJA_REAKCE,
      header: "První ráno",
      content: `
        <div class="text-center">               
           <p>No tak Ájo vidíš jakého máš chlapa! tohle je první věc, na kterou po svatební noci myslí.</p>
        </div>
`,
      navigation: [
         createNav("No, nechme to stranou a podívejme se, co běží v televizi", FstChapterId.STEP__PRVNI_RANO__TV),
      ],
      meta: {},
   },
   {
      id: FstChapterId.STEP__PRVNI_RANO__TV,
      imageId: ImageId.TV,
      navigation: [createContinueNav(FstChapterId.STEP__PRVNI_RANO__TV_DECIDE)],
      meta: { label: "TV" },
   },
   {
      id: FstChapterId.STEP__PRVNI_RANO__TV_DECIDE,
      navigation: [
         createNav("Zkusím jiný kanál", FstChapterId.STEP__PRVNI_RANO__TV),
         createNav("No televize už bylo dost", FstChapterId.STEP__END),
      ],
      meta: { label: "TV decide next" },
   },
   {
      id: FstChapterId.STEP__END,
      componentId: ComponentId.CHAPTER_END,
      navigation: [],
      meta: {
         type: "output",
         label: "End",
      },
   },
];
