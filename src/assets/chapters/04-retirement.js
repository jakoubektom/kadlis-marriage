import { createContinueNav, createNav } from "../chapterUtils";
import { ImageId } from "../references";

export const RetirementChapterId = {
   STEP__INIT: "301",
   STEP__INIT_IMG: "302",
   STEP__KRAJINA: "303",
   STEP__SMRT_FLOW: "307",
   STEP__END: "350",
};

export const retirementChapter = [
   {
      id: RetirementChapterId.STEP__INIT,
      header: "Panta rhei",
      content: `<div class="text-center">
<p>Čas neúprosně plyne a i vy se jednou dostanete až sem</p>
    <p>Až sem a spolu.</p>
    </div>
`,
      navigation: [createContinueNav(RetirementChapterId.STEP__INIT_IMG)],
      meta: {
         type: "input",
      },
   },
   {
      id: RetirementChapterId.STEP__INIT_IMG,
      imageId: ImageId.GRANDPARENTS,
      navigation: [createContinueNav(RetirementChapterId.STEP__KRAJINA)],
      meta: {
         label: "grandparents img",
      },
   },
   {
      id: RetirementChapterId.STEP__KRAJINA,
      content: `<div class="text-center">
<p>Sedíte po těch letech jako téměř už součást krajiny v trávě a shlížíte na to čím jste si společně prošli.</p>
<p>Nic naplat, bylo to dobré</p> 
</div>
`,
      imageId: ImageId.COUNTRYSIDE,
      navigation: [createContinueNav(RetirementChapterId.STEP__SMRT_FLOW)],
      meta: {
         label: "Vyhled na krajinu, vzpominani",
      },
   },
   ...createLinearFlow(RetirementChapterId.STEP__SMRT_FLOW, RetirementChapterId.STEP__END, [
      {
         content: `<div class="text-center">
                        <p>Všeokolní šepot nenápadně ustává a náhle to oba uslyšíte.</p>
                        </div>
`,
      },
      { content: `PĚKNÝ PODVEČER`, answer: "Zdravíme tě příteli, očekávali jsme tě" },
      { content: `ANO`, answer: "Co bude teď?" },
      { content: `JÁ`, answer: "A ještě něco jiného?" },
      {
         content: `<div class="text-center">
                            <p>PROMIŇTE, NEROZUMÍM</p> 
                        </div>
`,
         answer: "Můžeme jít spolu?",
      },
      { content: `ANO` },
   ]),
   {
      id: RetirementChapterId.STEP__END,
      content: `<div class="text-center">
<p>Přejeme příjemný život!</p>
<i>Pokud budete cokoliv potřebovat, jsme tady pro vás.</i>
<p>Vaši přátelé</p>
</div>`,
      navigation: [],
      meta: {
         label: "grandparents img",
      },
   },
];

function createLinearFlow(fstId, outId, flow) {
   const getId = (key) => `gen-${fstId}-${key}`;
   const generatedFlowSteps = flow.map(({ content, answer }, key) => ({
      id: getId(key),
      content: content,
      navigation: [answer ? createNav(answer, getId(key + 1)) : createContinueNav(getId(key + 1))],
      meta: {
         label: getId(key),
      },
   }));
   generatedFlowSteps[0].id = fstId;
   generatedFlowSteps[generatedFlowSteps.length - 1].navigation[0].goTo = outId;
   return generatedFlowSteps;
}
