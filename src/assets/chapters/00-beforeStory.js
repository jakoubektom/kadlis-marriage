
import { ComponentId, ImageId } from "../references";
import { Gifs } from "../pics";
import { createContinueNav, createNav } from "../chapterUtils";

export const BeforeStoryChapterId = {
    STEP_INIT: "900",
    STEP_INIT_DOWNLOAD: "901",
    STEP_START_TEXT: "902",
};

export const beforeStoryChapter = [
{
    id: BeforeStoryChapterId.STEP_INIT,
    header: "Cesta tam a už ne zpátky",
    content: "Vítejte na začátku své cesty, jen jeden dva kliky a můžete vykročit",
    navigation: [createNav("Vydat se na společnou cestu", BeforeStoryChapterId.STEP_INIT_DOWNLOAD)],
    meta: {
       type: "input",
    },
},   
{
    id: BeforeStoryChapterId.STEP_INIT_DOWNLOAD,
    componentId: ComponentId.LOADING_BAR,
    header: "Downloading virus Marriage",
    navigation: [createContinueNav(BeforeStoryChapterId.STEP_START_TEXT)],
    meta: {},

},
{
    id: BeforeStoryChapterId.STEP_START_TEXT,
    header: "Virus stažen",
    navigation: [],
    content: " Vraťte se zpátky, čeká na vás antivirový program",
    meta: {},
},
]
