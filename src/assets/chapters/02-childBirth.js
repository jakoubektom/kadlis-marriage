import { ComponentId, ImageId } from "../references";
import { createContinueNav, createNav } from "../chapterUtils";

export const ChildBirthChapterId = {
   STEP__POROD: "101",
   STEP_JIZDA: "110",
   STEP_VYHODNOCENI: "111",
   STEP__JIZDA_HOVADO: "102",
   STEP__JIZDA_PROFIK: "103",
   STEP__DalsiDeti: "104",
   STEP__OSUD_SE_NEPTA_DETI_BUDOU: "105",
   STEP_8__dvojcata: "106",
   STEP_END: "199",
};

export const childBirthChapter = [
   {
      id: ChildBirthChapterId.STEP__POROD,
      header: "POROD",
      content: "Ondro nastala tvá chvíle musíš Áju odvézt co nejrychleji do nemocnice, přepni na plochu 2",
      navigation: [createContinueNav(ChildBirthChapterId.STEP_JIZDA)],
      meta: {
         type: "input",
         label: "POROD",
      },
   },
   {
      id: ChildBirthChapterId.STEP_JIZDA,
      navigation: [createContinueNav(ChildBirthChapterId.STEP_VYHODNOCENI)],
      imageId: ImageId.AMBULANCE,
      meta: {
         label: "Ambulance",
      },
   },
   {
      id: ChildBirthChapterId.STEP_VYHODNOCENI,
      content: "Stihl jsi to, ale jel jsi jako...",
      navigation: [
         createNav("Hovado", ChildBirthChapterId.STEP__JIZDA_HOVADO),
         createNav("Profík", ChildBirthChapterId.STEP__JIZDA_PROFIK),
      ],
      meta: {
         label: "Ambulance",
      },
   },
   {
      id: ChildBirthChapterId.STEP__JIZDA_HOVADO,
      header: "Je to Holka",
      content:
         "Když vidíme jak nebezpečně řídíš, není asi dobré abys přivedl na svět dalšího takového mladíka," +
         " který bude za volantem ohrožovat svoje okolí. Takže se ti narodila holčička",
      navigation: [createContinueNav(ChildBirthChapterId.STEP__DalsiDeti)],
      meta: {},
   },
   {
      id: ChildBirthChapterId.STEP__JIZDA_PROFIK,
      header: "Je to Kluk",
      content: `<div class="text-center">
<p>Wow, zvládl jsi to jako skutečný profík, kdybychom odhlédli od toho, že Ája rodí, tak se měla jako v bavlnce.</p>
<p>Vyklubal se vám na svět kluk, nepochybně budoucí závodník.</p>
</div>
`,
      navigation: [createContinueNav(ChildBirthChapterId.STEP__DalsiDeti)],
      meta: {
         label: "Rychle",
      },
   },
   {
      id: ChildBirthChapterId.STEP__DalsiDeti,
      content: "Přejete si další Dítě",
      navigation: [
         createNav("Ano", ChildBirthChapterId.STEP_8__dvojcata),
         createNav("Ne", ChildBirthChapterId.STEP__OSUD_SE_NEPTA_DETI_BUDOU),
      ],
      meta: {
         label: "Přejete si další Dítě",
      },
   },
   {
      id: ChildBirthChapterId.STEP__OSUD_SE_NEPTA_DETI_BUDOU,
      header: "Ajejejej",
      content: `<div class="text-center">
<p>No osud se vás neptal, asi vám to někde uteklo</p>
<p>Čekáte další radost</p>

</div>`,
      navigation: [createContinueNav(ChildBirthChapterId.STEP_8__dvojcata)],
      meta: {},
   },
   {
      id: ChildBirthChapterId.STEP_8__dvojcata,
      header: "Překvápko",
      content: "Jejda! Doktor udělal chybku a místo jednoho vytáhl děti hned dvě",
      navigation: [createContinueNav(ChildBirthChapterId.STEP_END)],
      meta: {},
   },
   {
      id: ChildBirthChapterId.STEP_END,
      componentId: ComponentId.CHAPTER_END,
      navigation: [],
      meta: {
         type: "output",
         label: "End",
      },
   },
];
