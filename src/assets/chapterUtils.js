export function createContinueNav(targetStepId) {
	return createNav("Pokračovat", targetStepId);
}

export function createNav(label, targetStepId) {
	return { label: label, goTo: targetStepId };
}