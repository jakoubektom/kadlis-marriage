import { useConfigApi } from "../api/serverApi";
import { useCallback, useEffect, useState } from "react";

export const StringCompilerStrategy = {
   REPLACE_DIACRITICS: (string) => string,
   BASE: (string) =>
      string
         .replace(/ě/g, "e")
         .replace(/š/g, "s")
         .replace(/č/g, "c")
         .replace(/ř/g, "r")
         .replace(/ž/g, "z")
         .replace(/ď/g, "d")
         .replace(/ů/g, "u"),
};

export const useStringTemplateCompiler = () => {
   const configApi = useConfigApi();
   const [params, setParams] = useState(null);
   const compileTemplate = useCallback(
      (templateString, stringCompilerStrategy = StringCompilerStrategy.BASE) => {
         if (params) {
            if (!templateString) {
               return templateString;
            }
            const templatedString = templateString.replace(/#{(\w+)}/g, (_, property) => {
               const value = params[property];
               if (value === undefined) {
                  console.error(`Unable to find params[${property}] in `, params);
                  return property;
               }
               return value;
            });
            return stringCompilerStrategy(templatedString);
         } else {
            console.error("Params not initialized");
            return "";
         }
      },
      [params]
   );

   const fetchParams = async () => {
      const receivedParams = await configApi.getParams();
      setParams(receivedParams);
   };

   useEffect(() => {
      // noinspection JSIgnoredPromiseFromCall
      fetchParams();
   }, []);

   return params ? compileTemplate : null;
};
