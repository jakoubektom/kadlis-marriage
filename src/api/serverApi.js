import axios from "axios";

const api = axios.create({
   baseURL: "https://6283493638279cef71d420d2.mockapi.io",
   timeout: 1000,
});

const DEFAULT_PARAMS = {};

export const useConfigApi = () => {
   const url = "config";

   return {
      getParams: async () => {
         try {
            const response = await api.get(url);
            const data = response.data;
            const mappedData = data.params.reduce((res, param) => ({ ...res, [param.id]: param.value }), {});

            console.log("Received data: ", data, "\nParsed params: ", mappedData);

            return mappedData;
         } catch (e) {
            console.error("Failed receiving params ", e);
            return DEFAULT_PARAMS;
         }
      },
   };
};
