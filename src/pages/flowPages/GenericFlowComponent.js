import React from "react";
import { arrayOf, shape, string } from "prop-types";
import { PageSpinner } from "../../components/PageSpinner";
import { FlowPageButtonBar, FlowPageHeader } from "./FlowPageBaseComponents";
import { Button } from "reactstrap";
import { StringCompilerStrategy, useStringTemplateCompiler } from "../../service/useStringTemplateCompiler";
import { useStepFlowNavigate } from "./useStepFlowNavigate";
import { FlowImage } from "./FlowImage";

export const GenericFlowComponent = ({ model }) => {
   const compileTemplate = useStringTemplateCompiler();
   const goToStep = useStepFlowNavigate();
   const record = compileTemplate && {
      ...model,
      header: compileTemplate(model.header),
      content: compileTemplate(model.content, StringCompilerStrategy.REPLACE_DIACRITICS),
      navigation: model.navigation.map((nav) => ({
         ...nav,
         label: compileTemplate(nav.label, StringCompilerStrategy.REPLACE_DIACRITICS),
      })),
   };

   return record ? (
      <>
         {record.header && <FlowPageHeader>{record.header}</FlowPageHeader>}
         {record.content && (
            <div className={"align-self-center mt-4 mb-4 "} dangerouslySetInnerHTML={{ __html: record.content }} />
         )}
         {record.imageId && (
            <div className={"d-flex justify-content-center"}>
               <FlowImage code={record.imageId} />
            </div>
         )}
         <FlowPageButtonBar>
            {record.navigation.map((nav, key) => (
               <Button className={"ms-2 me-2"} onClick={() => goToStep(nav.goTo)} key={key} size={"lg"} outline>
                  {nav.label}
               </Button>
            ))}
         </FlowPageButtonBar>
      </>
   ) : (
      <PageSpinner display={true} />
   );
};

GenericFlowComponent.propTypes = {
   model: shape({
      header: string,
      content: string,
      navigation: arrayOf(
         shape({
            label: string,
            goTo: string,
         })
      ).isRequired,
   }).isRequired,
};
