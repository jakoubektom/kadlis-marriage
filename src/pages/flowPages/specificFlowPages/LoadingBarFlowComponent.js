import React, { useEffect, useState } from "react";
import { Button, Progress } from "reactstrap";
import { FlowPageButtonBar, FlowPageHeader } from "../FlowPageBaseComponents";
import { useStringTemplateCompiler } from "../../../service/useStringTemplateCompiler";
import { PageSpinner } from "../../../components/PageSpinner";
import { arrayOf, shape, string } from "prop-types";
import { useStepFlowNavigate } from "../useStepFlowNavigate";

function getRandomNumber(min, max) {
   return Math.floor(Math.random() * (max + 1 - min)) + min;
}

const LoadingBarFlowComponent = ({ header, navigation }) => {
   const [progressValue, setProgressValue] = useState(null);
   const goToStep = useStepFlowNavigate();
   const compileString = useStringTemplateCompiler();

   const incrementProgress = (curProgressValue) => {
      const res = curProgressValue + getRandomNumber(3, 8);
      setProgressValue(res > 100 ? 100 : res);
   };

   useEffect(() => {
      incrementProgress(0);
   }, []);

   useEffect(() => {
      if (progressValue > 0 && progressValue < 100) {
         setTimeout(() => {
            incrementProgress(progressValue);
         }, getRandomNumber(100, 600));
      }
   }, [progressValue]);

   return (
      <>
         <PageSpinner display={!compileString} />
         {compileString && (
            <>
               <FlowPageHeader>{compileString(header)}</FlowPageHeader>
               <Progress value={progressValue} />
               <FlowPageButtonBar>
                  {navigation.map((nav, key) => (
                     <Button
                        className={"ms-2 me-2"}
                        onClick={() => goToStep(nav.goTo)}
                        key={key}
                        size={"lg"}
                        outline
                        color={progressValue < 100 ? "primary" : "success"}
                        disabled={progressValue < 100}
                     >
                        {nav.label}
                     </Button>
                  ))}
               </FlowPageButtonBar>
            </>
         )}
      </>
   );
};

LoadingBarFlowComponent.propTypes = {
   header: string,
   navigation: arrayOf(
      shape({
         label: string,
         goTo: string,
      })
   ).isRequired,
};

export default LoadingBarFlowComponent;
