import React from "react";
import { FlowPageButtonBar } from "../FlowPageBaseComponents";
import { Button } from "reactstrap";
import { useNavigate } from "react-router-dom";

export const NotFoundStepComponent = () => {
   const navigate = useNavigate();
   return (
      <>
         <h1 className={"align-self-center display-3"}>Nenalezeno</h1>
         <FlowPageButtonBar>
            <Button
               // className={"ms-2 me-2"}
               onClick={() => navigate(-1)}
               size={"lg"}
               outline
            >
               Zpět
            </Button>
            <Button
               // className={"ms-2 me-2"}
               onClick={() => navigate("/")}
               size={"lg"}
               outline
            >
               Domů
            </Button>
         </FlowPageButtonBar>
      </>
   );
};

NotFoundStepComponent.propTypes = {};
