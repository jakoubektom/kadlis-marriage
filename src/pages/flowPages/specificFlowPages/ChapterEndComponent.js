import React from "react";
import { Button } from "reactstrap";
import { useNavigate } from "react-router-dom";
import {FlowPageHeader} from "../FlowPageBaseComponents";

export const ChapterEndComponent = () => {
   const navigate = useNavigate();
   return (
       <>
           <FlowPageHeader>Konec kapitoly</FlowPageHeader>
      <div className={"text-center"}>
         <Button
            // className={"ms-2 me-2"}
            onClick={() => navigate("/")}
            size={"lg"}
            outline
         >
            Domů
         </Button>
      </div>
       </>
   );
};

ChapterEndComponent.propTypes = {};
