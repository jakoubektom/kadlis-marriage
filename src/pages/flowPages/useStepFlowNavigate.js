import { useNavigate } from "react-router-dom";
import { useCallback } from "react";
import { StepIdConverter } from "../../utils/StepIdConverter";

export const useStepFlowNavigate = () => {
   const navigate = useNavigate();
   return useCallback(
      (stepId) => {
         navigate(`/step/${StepIdConverter.encode(stepId)}`);
      },
      [navigate]
   );
};
