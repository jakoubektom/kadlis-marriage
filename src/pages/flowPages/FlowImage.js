import React from "react";
import { string } from "prop-types";
import { ImageId } from "../../assets/references";
import friends from "../../assets/images/friends.jpeg";
import grandparents from "../../assets/images/grandparents.webp";
import countryside from "../../assets/images/countryside.webp";
import tv from "../../assets/images/tv.jpg";
import ambulance from "../../assets/images/ambulance.jpg";
import styled from "styled-components";

const Img = styled.img`
   max-height: 50vh;
   max-width: 50vw;
   border-radius: 3px;
`;

export const FlowImage = ({ code }) => {
   if (code === ImageId.FRIENDS) {
      return <Img src={friends} alt={code} />;
   } else if (code === ImageId.GRANDPARENTS) {
      return <Img src={grandparents} alt={code} />;
   } else if (code === ImageId.COUNTRYSIDE) {
      return <Img src={countryside} alt={code} />;
   } else if (code === ImageId.TV) {
      return <Img src={tv} alt={code} />;
   } else if (code === ImageId.AMBULANCE) {
      return <Img src={ambulance} alt={code} />;
   }
   return <p>Img {code}</p>;
};

FlowImage.propTypes = {
   code: string.isRequired,
};
