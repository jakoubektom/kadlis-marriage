import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { Container } from "reactstrap";

export const FlowPageHeader = ({ children, className = "" }) => (
   <h1 className={`align-self-center mb-4 display-3 ${className}`}>{children}</h1>
);
FlowPageHeader.propTypes = {
   children: PropTypes.string,
   className: PropTypes.string,
};

const FlowPageStyledContainer = styled(Container)`
   font-size: 1.6em;
   font-weight: normal;
`;

export const FlowPageContainer = ({ children, className = "" }) => (
   <FlowPageStyledContainer className={`d-flex flex-column justify-content-center ${className}`}>
      {children}
   </FlowPageStyledContainer>
);

FlowPageContainer.propTypes = {
   children: PropTypes.node,
   className: PropTypes.string,
};

const FlowPageButtonBarContainer = styled.div`
   button {
      margin: 0 1em 0 1em;
   }
`;
export const FlowPageButtonBar = ({ children, className = "" }) => (
   <FlowPageButtonBarContainer className={`d-flex justify-content-center mt-4 ${className}`}>
      {children}
   </FlowPageButtonBarContainer>
);
FlowPageButtonBar.propTypes = {
   children: PropTypes.node,
   className: PropTypes.string,
};
