import React, { useMemo, useState } from "react";
import { useParams } from "react-router-dom";
import { ComponentId } from "../../assets/references";
import { PageSpinner } from "../../components/PageSpinner";
import { StepIdConverter } from "../../utils/StepIdConverter";
import { FlowPageContainer } from "./FlowPageBaseComponents";
import { GenericFlowComponent } from "./GenericFlowComponent";
import LoadingBarFlowComponent from "./specificFlowPages/LoadingBarFlowComponent";
import { story } from "../../assets/story";
import { ChapterEndComponent } from "./specificFlowPages/ChapterEndComponent";
import { NotFoundStepComponent } from "./specificFlowPages/NotFoundStepComponent";

export const GenericFlowPage = () => {
   const { id: urlId } = useParams();
   const id = StepIdConverter.decode(urlId);
   const [notFoundStep, setNotFoundStep] = useState(!id);

   const model = useMemo(() => {
      if (id != null) {
         const dataSets = story.filter((step) => step.id === id);
         if (dataSets.length === 0) {
            setNotFoundStep(true);
            console.error(`No record for step '${id}' found`);
            return null;
         } else if (dataSets.length > 1) {
            throw Error(`Multiple records for step '${id}' found`);
         }
         setNotFoundStep(false);
         return dataSets[0];
      } else {
         return null;
      }
   }, [id]);

   console.log("model", model);

   return (
      <FlowPageContainer className={"d-flex flex-column justify-content-center "}>
         {notFoundStep && <NotFoundStepComponent />}
         <PageSpinner display={!notFoundStep && !model} />
         {model && (
            <>
               {(!model.componentId || model.componentId === ComponentId.GENERIC) && (
                  <GenericFlowComponent model={model} />
               )}
               {model.componentId === ComponentId.LOADING_BAR && (
                  <LoadingBarFlowComponent header={model.header} navigation={model.navigation} />
               )}
               {model.componentId === ComponentId.CHAPTER_END && <ChapterEndComponent />}
            </>
         )}
      </FlowPageContainer>
   );
};

GenericFlowPage.propTypes = {};
