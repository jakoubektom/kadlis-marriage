import React from "react";
import { useNavigate } from "react-router-dom";
import { Button, Container } from "reactstrap";
import { useStepFlowNavigate } from "./flowPages/useStepFlowNavigate";
import {PART_0_START, PART_1_START, PART_2_START, PART_4_START} from "../assets/externalReferences";

export const HomePage = () => {
   const navigate = useNavigate();
   const goToFlowStep = useStepFlowNavigate();

   return (
      <Container className={"d-flex flex-column mt-4"}>
         <h1 className={"align-self-center display-1"}>Vase svatba</h1>
         <div className={"align-self-center"}>
            <Button className={"me-4"} onClick={() => goToFlowStep(PART_0_START)}>
               Úvod - pouze pro Áju a Ondru
            </Button>
            <Button className={"me-4"} onClick={() => goToFlowStep(PART_1_START)}>
               Kapitola 1
            </Button>
            <Button className={"me-4"} onClick={() => goToFlowStep(PART_2_START)}>
               Kapitola 2
            </Button>
            <Button className={"me-4"} onClick={() => goToFlowStep(PART_4_START)}>
               Závěr
            </Button>
            <Button onClick={() => navigate("graph")} outline>Obsah</Button>
         </div>
      </Container>
   );
};

HomePage.propTypes = {};

export default HomePage;
