import styled from "styled-components";
import { Route, Routes } from "react-router-dom";
import { FlowChart } from "./components/FlowChart";
import { HomePage } from "./pages/HomePage";
import { GenericFlowPage } from "./pages/flowPages/GenericFlowPage";
import NotFoundPage from "./pages/NotFoundPage";
import { Color } from "./utils/Constants";

const Container = styled.div`
   height: 100%;
   display: flex;
   background-color: ${Color.BACKGROUND_DEFAULT};
`;

export const App = () => {
   return (
      <Container>
         <Routes>
            <Route path={"/"} element={<HomePage />} exact />
            <Route path={"step"}>
               <Route path={":id"} element={<GenericFlowPage />} />
            </Route>
            <Route path={"graph"} element={<FlowChart />} />
            <Route path={"/*"} element={<NotFoundPage />} />
         </Routes>
      </Container>
   );
};
